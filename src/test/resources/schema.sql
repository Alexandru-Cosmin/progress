--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3
-- Dumped by pg_dump version 10.3


--
-- Name: workschema; Type: SCHEMA; Schema: -; Owner: alex
--



CREATE SCHEMA workschema;


CREATE TABLE workschema.task (
    id integer NOT NULL AUTO_INCREMENT,
    name character varying(20) NOT NULL,
    description character varying(20) NOT NULL,
    completed_final_flag boolean DEFAULT false
);

--
-- Name: task_to_user; Type: TABLE; Schema: workschema; Owner: alex
--

CREATE TABLE workschema.task_to_user (
    user_id integer NOT NULL,
    task_id integer NOT NULL,
    completed_flag boolean NOT NULL,
    percentage_of_completion integer NOT NULL,
    id integer NOT NULL
);


--
-- Name: user; Type: TABLE; Schema: workschema; Owner: alex
--

CREATE TABLE workschema.user (
    id integer NOT NULL,
    name character varying(20) NOT NULL,
    password character varying(20) NOT NULL,
    admin boolean NOT NULL
);

