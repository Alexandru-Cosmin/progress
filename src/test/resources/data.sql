--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3
-- Dumped by pg_dump version 10.3

--
-- Data for Name: task; Type: TABLE DATA; Schema: workschema; Owner: alex
--

INSERT INTO workschema.task VALUES (1, 'login', 'Make login page', false);
INSERT INTO workschema.task VALUES (2, 'product', 'Make product page', false);
INSERT INTO workschema.task VALUES (3, 'deleteButton', 'Make delete button', false);
INSERT INTO workschema.task VALUES (15, 'asdad', 'sadfasdfasdf', false);
INSERT INTO workschema.task VALUES (4, 'logout', 'make logout function', true);


--
-- Data for Name: user; Type: TABLE DATA; Schema: workschema; Owner: alex
--

INSERT INTO workschema.user VALUES (1, 'Mike','123', false);
INSERT INTO workschema.user VALUES (2, 'Jim','123', false);
INSERT INTO workschema.user VALUES (3, 'Bill','123', true);


--
-- Data for Name: task_to_user; Type: TABLE DATA; Schema: workschema; Owner: alex
--

INSERT INTO workschema.task_to_user VALUES (3, 4, false, 20, 4);
INSERT INTO workschema.task_to_user VALUES (3, 1, false, 50, 2);
INSERT INTO workschema.task_to_user VALUES (2, 1, true, 100, 5);
INSERT INTO workschema.task_to_user VALUES (2, 3, false, 25, 3);
INSERT INTO workschema.task_to_user VALUES (1, 2, false, 25, 1);



--
-- PostgreSQL database dump complete
--

