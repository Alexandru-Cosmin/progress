package com.training.progress.repository;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.training.progress.ProgressApplication;
import com.training.progress.model.Task;
import com.training.progress.model.User;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProgressApplication.class)
public class TestDatabaseManagerJdbc {

	public static final String SQL_INSERT_USERS = "INSERT INTO user "
			+ "(name, admin) VALUES ('Mike',false),('Jim',false),('Bill',true)";
	
	
	@Autowired
	@Qualifier("databaseManagerJdbc")
	DatabaseManager databaseManager;
	
	@Test
	public void getUsersTest() {
		List<User> users = databaseManager.getUsers();

		Assert.assertNotNull(users);
		Assert.assertEquals(users.get(0).getName(), "Mike");
		Assert.assertEquals(users.get(0).isAdmin(), false);
		Assert.assertEquals(users.get(1).getName(), "Jim");
		Assert.assertEquals(users.get(1).isAdmin(), false);
		Assert.assertEquals(users.get(2).getName(), "Bill");
		Assert.assertEquals(users.get(2).isAdmin(), true);
	}
	
	@Test
	public void getTasksByUserIdTest() {
		List<Task> tasks = databaseManager.getTasksByUserId(3);

		Assert.assertNotNull(tasks);
		Assert.assertEquals(tasks.size(), 2);
		Assert.assertEquals(tasks.get(0).getName(), "login");
		Assert.assertEquals(tasks.get(0).getDescription(), "Make login page");
		Assert.assertEquals(tasks.get(1).getName(), "logout");
		Assert.assertEquals(tasks.get(1).getDescription(), "make logout function");
	}
	
	@Test
	public void findPercentageOfCompletionTest() {
		int percentageOfCompletion = databaseManager.findPercentageOfCompletion(3, 1);

		Assert.assertEquals(percentageOfCompletion, 50);
	}
	
	@Test
	public void setPercentageOfCompletionTest() {
		String operationMessage = databaseManager.setPercentageOfCompletion(3, 1, 78);
		Assert.assertEquals(operationMessage,"success");
	}
	
	@Test
	public void addTaskTest() {
		String operationMessage = databaseManager.addTask(3, "testTask", "test");
		Assert.assertEquals("success",operationMessage);
	}
	
	@Test
	public void deleteTaskTest() {
		String operationMessage = databaseManager.deleteTask(3, 1);
		Assert.assertEquals("success",operationMessage);
	}
	
	
	@Test
	public void checkHowManyTimesTaskWasCompletedTest() {
		String operationMessage = databaseManager.checkHowManyTimesTaskWasCompleted(3, 2);
		Assert.assertEquals("Task was assigned 1 times and was completed 0 times!",operationMessage);
	}
	
	@Test
	public void userIsAdminTest() {
		boolean isAdmin = databaseManager.userIsAdmin(3);
		Assert.assertEquals(true,isAdmin);
	}
}
