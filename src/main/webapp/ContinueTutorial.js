/**
 * 
 */
var data = {
		globalName:'Yoshi'
}

Vue.component('greeting',{
	template:'<p>This is the local name from component: {{ localName }} and this is '+
		' the global name {{ globalName }} <button v-on:click="changeGlobalName">ChangeGlobalName</button></p>',
	data: function(){
		return data;
	},
	methods:{
		changeGlobalName: function(){
			this.globalName = 'Nagasaki';
		}
	}
})

new Vue({
	el: '#div1',
	data:{
		name:'Vue1'
	},
	methods:{
		logRefValue : function(){
			console.log(this.$refs.myRef.value);
		}
	}
});

new Vue({
	el: '#div2',
	data:{
		name:'Vue2'
	}
});