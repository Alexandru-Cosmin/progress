/**
 * This is a vueJs file
 */
new Vue({
	el:'#app',
	data:{
		name : 'Alex',
		websiteTag: '<a href="https://www.sport.ro/">Sport.ro</a>',
		x: 0,
		y: 0,
		suma: 0,
		cssClass: 'red',
		counter: 0,
		available: false,
		green: false,
		text: false,
		persons: [{name:'Jim',age:30},
			{name:'Alex',age:23},
			{name:'Mike',age:39}],
		continueTutorial: 'ContinueTutorial.html'
	},
	methods:{
		sum: function(){
			console.log("Sum entered");
			this.suma = this.x * this.y;
			console.log("Sum" + this.suma);
		},
		click: function(){
			window.alert('Window alert!');
		},
		changeClass: function(){
			this.counter++;
			if(this.counter % 2 == 0){
				this.cssClass = 'blue';
			}else{
				this.cssClass = 'green';
			}
		},
		changeBackgroundColor: function(){
			this.green = !this.green;
			this.text = !this.text
		}
	},
	computed:{
		compClass: function(){
			return{
				green: this.green,
				text: this.text
			}
		}
	}
});