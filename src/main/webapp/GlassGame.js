/**
 * VueJs for GlassGame
 */

new Vue({
	el:'#appDiv1',
	data:{
		health: 100,
		imageSource: "../resources/glass.jpg",
		showAttackButton: true
	},
	methods:{
		attack: function(){
			var newValue = this.health - parseInt(Math.random()*10);
			if(newValue<0){
				this.health = 0;
				this.imageSource = "../resources/brokenGlass.jpg";
				this.showAttackButton = false;
			}else{
				this.health = newValue;
			}
		},
		reset: function(){
			this.health = 100;
			this.imageSource = "../resources/glass.jpg";
			this.showAttackButton = true;
		}
	},
	computed:{
		style:function(){
			return{
				width:this.health+"%"
			}
		}
	}
});