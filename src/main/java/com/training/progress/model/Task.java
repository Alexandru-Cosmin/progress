package com.training.progress.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="task", schema="workschema")
public class Task {

	@Id
	@SequenceGenerator(name="task_seq", sequenceName="task_id_seq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="task_seq")
	private int id;
	private String name;
	private String description;
	@Column(name = "completed_final_flag")
	private boolean completedFinalFlag;
	
	public Task() {
		super();
	}

	public Task(int id, String name, String description, boolean completedFinalFlag) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.completedFinalFlag = completedFinalFlag;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isCompletedFinalFlag() {
		return completedFinalFlag;
	}

	public void setCompletedFinalFlag(boolean completedFinalFlag) {
		this.completedFinalFlag = completedFinalFlag;
	}

	@Override
	public String toString() {
		return "Task [id=" + id + ", name=" + name + ", description=" + description + ", completedFinalFlag="
				+ completedFinalFlag + "]";
	}

	
	
}
