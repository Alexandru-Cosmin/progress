package com.training.progress.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="\"task_to_user\"", schema="\"workschema\"")
public class TaskToUser {

	@Id
	private int id;
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "userId")
	private User user;
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "taskId")
	private Task task;
	@Column(name = "completed_flag")
	private boolean completedFlag;
	@Column(name = "percentage_of_completion")
	private int percentageOfCompletion;

	

	public TaskToUser() {
		super();
	}



	public TaskToUser(int id, User user, Task task, boolean completedFlag, int percentageOfCompletion) {
		super();
		this.id = id;
		this.user = user;
		this.task = task;
		this.completedFlag = completedFlag;
		this.percentageOfCompletion = percentageOfCompletion;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}



	public Task getTask() {
		return task;
	}



	public void setTask(Task task) {
		this.task = task;
	}



	public boolean isCompletedFlag() {
		return completedFlag;
	}



	public void setCompletedFlag(boolean completedFlag) {
		this.completedFlag = completedFlag;
	}



	public int getPercentageOfCompletion() {
		return percentageOfCompletion;
	}



	public void setPercentageOfCompletion(int percentageOfCompletion) {
		this.percentageOfCompletion = percentageOfCompletion;
	}



	@Override
	public String toString() {
		return "TaskToUser [id=" + id + ", user=" + user + ", task=" + task + ", completedFlag=" + completedFlag
				+ ", percentageOfCompletion=" + percentageOfCompletion + "]";
	}



	
	
	
}
