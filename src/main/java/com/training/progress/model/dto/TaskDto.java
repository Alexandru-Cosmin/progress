package com.training.progress.model.dto;

public class TaskDto {

	private int id;
	private String name;
	private String description;
	private boolean completed_final_flag;
	
	public TaskDto(int id, String name, String description, boolean completed_final_flag) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.completed_final_flag = completed_final_flag;
	}
	public TaskDto() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isCompleted_final_flag() {
		return completed_final_flag;
	}
	public void setCompleted_final_flag(boolean completed_final_flag) {
		this.completed_final_flag = completed_final_flag;
	}
	@Override
	public String toString() {
		return "TaskDto [id=" + id + ", name=" + name + ", description=" + description + ", completed_final_flag="
				+ completed_final_flag + "]";
	}
	
	
}
