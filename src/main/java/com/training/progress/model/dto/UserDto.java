package com.training.progress.model.dto;

public class UserDto {

	private int id;
	private String name;
	private String password;
	private boolean isAdmin;
	
	public UserDto() {
		super();
	}

	public UserDto(int id, String name, String password, boolean isAdmin) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.isAdmin = isAdmin;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	@Override
	public String toString() {
		return "UserDto [id=" + id + ", name=" + name + ", password=" + password + ", isAdmin=" + isAdmin + "]";
	}
	
	
}
