package com.training.progress.model.dto;

public class CounterOfTask {

	private int assigned;
	private int completed;
	public CounterOfTask() {
		super();
	}
	public CounterOfTask(int assigned, int completed) {
		super();
		this.assigned = assigned;
		this.completed = completed;
	}
	public int getAssigned() {
		return assigned;
	}
	public void setAssigned(int assigned) {
		this.assigned = assigned;
	}
	public int getCompleted() {
		return completed;
	}
	public void setCompleted(int completed) {
		this.completed = completed;
	}
	@Override
	public String toString() {
		return "CounterOfTask [assigned=" + assigned + ", completed=" + completed + "]";
	}
	
	
}
