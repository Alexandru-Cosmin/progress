package com.training.progress.model.dto;

public class TaskToUserDto {

	private int id;
	// TODO Refactor camel case
	private int user_id;
	private int task_id;
	private boolean completed_flag;
	private int percentage_of_completion;
	public TaskToUserDto() {
		super();
	}
	public TaskToUserDto(int id, int user_id, int task_id, boolean completed_flag, int percentage_of_completion) {
		super();
		this.id = id;
		this.user_id = user_id;
		this.task_id = task_id;
		this.completed_flag = completed_flag;
		this.percentage_of_completion = percentage_of_completion;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getTask_id() {
		return task_id;
	}
	public void setTask_id(int task_id) {
		this.task_id = task_id;
	}
	public boolean isCompleted_flag() {
		return completed_flag;
	}
	public void setCompleted_flag(boolean completed_flag) {
		this.completed_flag = completed_flag;
	}
	public int getPercentage_of_completion() {
		return percentage_of_completion;
	}
	public void setPercentage_of_completion(int percentage_of_completion) {
		this.percentage_of_completion = percentage_of_completion;
	}
	@Override
	public String toString() {
		return "TaskToUser [id=" + id + ", user_id=" + user_id + ", task_id=" + task_id + ", completed_flag="
				+ completed_flag + ", percentage_of_completion=" + percentage_of_completion + "]";
	}
	
}
