package com.training.progress.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="user", schema="workschema")
public class User {

	@Id
	@SequenceGenerator(name="user_seq", sequenceName="user_id_seq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="user_seq")
	private int id;
	private String name;
	private String password;
	private boolean admin;
	public User() {
		super();
	}
	public User(int id, String name, String password, boolean admin) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.admin = admin;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", password=" + password + ", admin=" + admin + "]";
	}
	
	
	
}
