package com.training.progress.model.mapper;

import com.training.progress.model.User;
import com.training.progress.model.dto.UserDto;

public class UserMapper {

	public static UserDto mapUserToUserDto(User u) {
		UserDto userDto = new UserDto();
		userDto.setName(u.getName());
		userDto.setId(u.getId());
		userDto.setAdmin(u.isAdmin());
		userDto.setPassword(u.getPassword());
		return userDto;
	}

}
