package com.training.progress.model.mapper;

import com.training.progress.model.Task;
import com.training.progress.model.dto.TaskDto;

public class TaskMapper {

	public static TaskDto mapTaskToTaskDto(Task t) {
		TaskDto taskDto = new TaskDto();
		taskDto.setId(t.getId());
		taskDto.setName(t.getName());
		taskDto.setDescription(t.getDescription());
		taskDto.setCompleted_final_flag(t.isCompletedFinalFlag());
		return taskDto;
	}

	
}
