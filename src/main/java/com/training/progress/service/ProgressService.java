package com.training.progress.service;

import java.util.List;

import com.training.progress.model.User;
import com.training.progress.model.dto.TaskDto;
import com.training.progress.model.dto.UserDto;


public interface ProgressService {

	public List<UserDto> getUsers();
	public List<TaskDto> getTaskById(int id);
	public List<TaskDto> getTasksByUserId(int id);
	public String addTask(int userId, String name, String description);
	public String deleteTask(int userId, int id);
	public String getPercentageOfCompletionOfTask(int userId, int taskId);
	public String setPercentageOfCompletion(int userId, int taskId, int percentageOfCompletion);
	public String checkHowManyTimesTaskWasCompleted(int userId, int taskId);
	public boolean login(UserDto u);
}
