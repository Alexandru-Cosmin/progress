package com.training.progress.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.training.progress.model.Task;
import com.training.progress.model.User;
import com.training.progress.model.dto.TaskDto;
import com.training.progress.model.dto.UserDto;
import com.training.progress.model.mapper.TaskMapper;
import com.training.progress.model.mapper.UserMapper;
import com.training.progress.repository.DatabaseManager;

@Component
public class ProgressServiceImpl implements ProgressService{
	
	@Autowired
	/*@Qualifier("databaseManagerJpa")*/
	@Qualifier("databaseManagerJdbc")
	DatabaseManager databaseManager;
	
	private Logger log = LoggerFactory.getLogger(ProgressServiceImpl.class);

	@Override
	public ArrayList<UserDto> getUsers() {
		log.info("getUsers called in service");
		List<User> users = databaseManager.getUsers();
		ArrayList<UserDto> usersDto = new ArrayList<UserDto>();
		for(User u : users){
			UserDto userDto = UserMapper.mapUserToUserDto(u);
			usersDto.add(userDto);
		}
		log.info("Users returned in service: "+usersDto.toString());
		return usersDto;
	}

	@Override
	public List<TaskDto> getTaskById(int id) {
		log.info("getTasksByUserId called in service");
		List<Task> tasks = databaseManager.getTaskById(id);
		List<TaskDto> tasksDto = new ArrayList<TaskDto>();
		for(Task t : tasks){
			TaskDto taskDto = new TaskDto(t.getId(),t.getName(),t.getDescription(),t.isCompletedFinalFlag());
			tasksDto.add(taskDto);
		}
		log.info("Tasks returned in service: "+tasksDto.toString());
		return tasksDto;
	}

	@Override
	public List<TaskDto> getTasksByUserId(int id) {
		log.info("getTasksByUserId called in service");
		List<Task> tasks = databaseManager.getTasksByUserId(id);
		List<TaskDto> tasksDto = new ArrayList<TaskDto>();
		for(Task t : tasks){
			TaskDto taskDto = TaskMapper.mapTaskToTaskDto(t);
			tasksDto.add(taskDto);
		}
		log.info("Tasks returned in service: "+tasksDto.toString());
		return tasksDto;
	}

	@Override
	public String getPercentageOfCompletionOfTask(int userId,int taskId) {
		log.info("getPercentageOfCompletionOfTask called in service");
		int percentageOfCompletionOfTask = databaseManager.findPercentageOfCompletion(userId,taskId);
		log.info("PercentageOfCompletionOfTask returned in service: "+percentageOfCompletionOfTask);
		return percentageOfCompletionOfTask+"";
	}

	@Override
	public String setPercentageOfCompletion(int userId, int taskId, int percentageOfCompletion) {
		log.info("setPercentageOfCompletion called in service");
		String operationMessage = databaseManager.setPercentageOfCompletion(userId,taskId,percentageOfCompletion);
		return operationMessage;
	}

	@Override
	public String addTask(int userId, String name, String description) {
		log.info("addTask called in service");
		String operationMessage = "success";
		if(databaseManager.userIsAdmin(userId)){
			operationMessage = databaseManager.addTask(userId, name, description);
		}else{
			operationMessage = "User does not have the rights to add a task";
		}
		
		return operationMessage;
	}

	@Override
	public String deleteTask(int userId, int id) {
		log.info("deleteTask called in service");
		String operationMessage = "success";
		if(databaseManager.userIsAdmin(userId)){
			operationMessage = databaseManager.deleteTask(userId, id);
		}else{
			operationMessage = "User does not have the rights to delete a task";
		}
		return operationMessage;
	}
	
	@Override
	public String checkHowManyTimesTaskWasCompleted(int userId, int taskId) {
		log.info("checkHowManyTimesTaskWasCompleted called in service");
		String operationMessage = "success";
		if(databaseManager.userIsAdmin(userId)){
			operationMessage = databaseManager.checkHowManyTimesTaskWasCompleted(userId, taskId);
		}else{
			operationMessage = "User does not have the rights to check status of a task";
		}
		return operationMessage;
	}

	@Override
	public boolean login(UserDto u) {
		log.info("login called in service");
		boolean loginState = databaseManager.login(u);
		return loginState;
	}

}
