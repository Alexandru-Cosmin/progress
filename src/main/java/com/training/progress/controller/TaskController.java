package com.training.progress.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.training.progress.model.Task;
import com.training.progress.model.dto.TaskDto;
import com.training.progress.service.ProgressService;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/task")
public class TaskController {
	
	@Autowired
	ProgressService progressServiceImpl;

	private Logger log = LoggerFactory.getLogger(TaskController.class);

	/* http://localhost:8080/task/getTasks?id=3 */
	@RequestMapping(value="/getTasks", method=RequestMethod.GET)
	public List<TaskDto> getTasksByUserId(@RequestParam int id) {
		log.info("getTasksByUserId called in controller");
		List<TaskDto> tasks = progressServiceImpl.getTasksByUserId(id);
		log.info("Tasks returned in controller: " + tasks.toString());
		return tasks;
	}
	
	/* http://localhost:8080/task?userId=2&taskId=1 */
	@RequestMapping(method=RequestMethod.GET)
	public String getPercentageOfCompletionOfTask(@RequestParam int userId, @RequestParam int taskId) {
		log.info("getPercentageOfCompletionOfTask called in controller");
		String percentageOfCompletion = progressServiceImpl.getPercentageOfCompletionOfTask(userId, taskId);
		log.info("percentageOfCompletion returned in controller: " + percentageOfCompletion);
		return percentageOfCompletion;
	}
	
	/* http://localhost:8080/task?taskId=1&percentageOfCompletion=25&userId=3 */
	@RequestMapping(method=RequestMethod.POST)
	public String setPercentageOfCompletion(@RequestParam int taskId,
			@RequestParam int percentageOfCompletion, @RequestParam int userId) {
		log.info("setPercentageOfCompletion called in controller");
		String operationMessage = progressServiceImpl.setPercentageOfCompletion(userId, taskId, percentageOfCompletion);
		return operationMessage;
	}
	
	/* http://localhost:8080/task/addTask?userId=3 */
	@RequestMapping(value="/addTask", method=RequestMethod.POST)
	public String addTask(@RequestParam int userId, @RequestBody Task task) {
		log.info("addTask called in controller");
		String operationMessage = progressServiceImpl.addTask(userId, task.getName(), task.getDescription());
		return operationMessage;
	}
	
	/* http://localhost:8080/task?taskId=2&userId=3 */
	@Modifying
	@Transactional
	@RequestMapping( method=RequestMethod.DELETE)
	public String deleteTask(@RequestParam int taskId, @RequestParam int userId) {
		log.info("addTask called in controller");
		String operationMessage = progressServiceImpl.deleteTask(userId, taskId);
		return operationMessage;
	}
	
	/* http://localhost:8080/task/checkHowManyTimesTaskWasCompleted?userId=3&taskId=2 */
	@RequestMapping(value = "/checkHowManyTimesTaskWasCompleted", method = RequestMethod.POST)
	public String checkHowManyTimesTaskWasCompleted(@RequestParam int userId, @RequestParam int taskId) {
		log.info("checkHowManyTimesTaskWasCompleted called in controller");
		String operationMessage = progressServiceImpl.checkHowManyTimesTaskWasCompleted(userId, taskId);
		return operationMessage;
	}
}
