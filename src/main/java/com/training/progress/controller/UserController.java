package com.training.progress.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.training.progress.model.User;
import com.training.progress.model.dto.UserDto;
import com.training.progress.service.ProgressService;

@RestController
@CrossOrigin(origins = "http://localhost:8080") 
@RequestMapping("/user")
public class UserController {

	@Autowired
	ProgressService progressServiceImpl;

	private Logger log = LoggerFactory.getLogger(UserController.class);
	
	/* http://localhost:8081/user */
	@RequestMapping(method=RequestMethod.GET)
	public List<UserDto> getUsers() {
		log.info("getUsers called in controller");
		List<UserDto> users = progressServiceImpl.getUsers();
		log.info("Users returned in controller: " + users.toString());
		return users;
	}
	
	/* http://localhost:8081/user/login */
	@RequestMapping(value="/login" ,method=RequestMethod.POST)
	public boolean login(@RequestBody UserDto u) {
		log.info("getUsers called in controller");
		boolean loginState = progressServiceImpl.login(u);
		return loginState;
	}
}
