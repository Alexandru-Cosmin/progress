package com.training.progress.repository;

import java.util.List;

import com.training.progress.model.Task;
import com.training.progress.model.User;
import com.training.progress.model.dto.UserDto;

public interface DatabaseManager {

	public List<User> getUsers();
	public List<Task> getTaskById(int id);
	public List<Task> getTasksByUserId(int id);
	public int findPercentageOfCompletion(int userId,int taskId);
	public String setPercentageOfCompletion(int userId, int taskId, int percentageOfCompletion);
	public String addTask(int userId, String name, String description);
	public String deleteTask(int userId, int id);
	public String checkHowManyTimesTaskWasCompleted(int userId, int taskId);
	public boolean userIsAdmin(int userId);
	public boolean login(UserDto u);
}
