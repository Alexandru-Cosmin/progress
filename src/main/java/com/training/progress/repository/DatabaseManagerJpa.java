package com.training.progress.repository;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.training.progress.model.Task;
import com.training.progress.model.TaskToUser;
import com.training.progress.model.User;
import com.training.progress.model.dto.UserDto;

@Component
public class DatabaseManagerJpa implements DatabaseManager{

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TaskRepository taskRepository;
	
	@Autowired
	private TaskToUserRepository taskToUserRepository;
	
	private Logger log = LoggerFactory.getLogger(DatabaseManagerJpa.class);
	
	@Override
	public List<User> getUsers() {
		log.info("Get users in DBMJpa");
		return (ArrayList<User>) userRepository.findAll();
	}

	@Override
	public List<Task> getTaskById(int id) {
		log.info("getTasksByUserId called in DBMJpa");
		log.info("User id: "+id);
		List<Integer> ids = new ArrayList<Integer>();
		ids.add(id);
		List<Task> tasks = new ArrayList<Task>();
		try {
			tasks	= (ArrayList<Task>) taskRepository.findAllById(ids);
		}catch(DataAccessException e) {
			log.error(e.getMessage());
		}
		log.info("Tasks returned in DBMJpa: "+tasks.toString());
		return tasks;
	}
	
	@Override
	public List<Task> getTasksByUserId(int id) {
		log.info("getTasksByUserId called in DBMJpa");
		List<Task> tasksOfUser = new ArrayList<Task>();
		List<TaskToUser> taskToUserList = new ArrayList<TaskToUser>();
		try {
			taskToUserList	= (ArrayList<TaskToUser>) taskToUserRepository.findAllElementsByUserId(id);
			for(TaskToUser tt : taskToUserList){
				tasksOfUser.add(tt.getTask());
				System.out.println("Task to user: "+tt.toString());
			}
		}catch(DataAccessException e) {
			log.error(e.getMessage());
		}
		log.info("Tasks returned in DBMJpa: "+tasksOfUser.toString());
		return tasksOfUser;
	}

	@Override
	public int findPercentageOfCompletion(int userId,int taskId) {
		log.info("findPercentageOfCompletion called in DBMJpa");
		int percentageOfCompletion = taskToUserRepository.findPercentageOfCompletionOfTask(userId, taskId);
		return percentageOfCompletion;
	}

	@Override
	public String setPercentageOfCompletion(int userId, int taskId, int percentageOfCompletion) {
		log.info("findPercentageOfCompletion called in DBMJpa");
		String operationMessage = "success";
		try{
			taskToUserRepository.setPercentageOfCompletion(percentageOfCompletion, userId, taskId);
		}catch(DataAccessException e) {
			operationMessage = "fail";
			log.error(e.getMessage());
		}
		return operationMessage;
	}

	
	@Override
	public String addTask(int userId, String name, String description) {
		log.info("addTask called in DBMJpa");
		String operationMessage = "success";
		try{
				taskRepository.addTask(name,description);
		}catch(DataAccessException e) {
			operationMessage = "fail";
			log.error(e.getMessage());
		}
		return operationMessage;
	}

	// TODO validation goes to logic, in repository/dao, only related to DB access
	@Override
	public String deleteTask(int userId, int id) {
		log.info("deleteTask called in DBMJpa");
		String operationMessage = "success";
		try{
			taskRepository.deleteById(id);
		}catch(DataAccessException e) {
			operationMessage = "fail";
			log.error(e.getMessage());
		}
		return operationMessage;
	}

	@Override
	public String checkHowManyTimesTaskWasCompleted(int userId, int taskId) {
		log.info("checkHowManyTimesTaskWasCompleted called in DBMJpa");
		String operationMessage = "success";
		int assignedCounter = 0;
		ArrayList<TaskToUser> taskToUserList = new ArrayList<TaskToUser>();
		try{
				taskToUserList = (ArrayList<TaskToUser>) taskToUserRepository.checkHowManyTimesTaskWasCompleted(taskId);
				assignedCounter = taskToUserList.size();
				operationMessage = "Task was assigned "+assignedCounter+" times and was completed "
						+countCompleted(taskToUserList)+" times!";
		}catch(DataAccessException e) {
			operationMessage = "fail";
			log.error(e.getMessage());
		}
		return operationMessage;
	}
	
	private int countCompleted(ArrayList<TaskToUser> taskToUserList) {
		int completedCounter = 0;
		for(TaskToUser t : taskToUserList){
			if(t.isCompletedFlag()){
				completedCounter++;
			}
		}
		return completedCounter;
	}

	

	@Override
	public boolean userIsAdmin(int userId) {
		User u = userRepository.findUserById(userId);
		return u.isAdmin();
	}

	@Override
	public boolean login(UserDto u) {
		// TODO Auto-generated method stub
		return false;
	}

}
