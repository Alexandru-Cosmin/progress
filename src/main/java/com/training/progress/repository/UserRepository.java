package com.training.progress.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.training.progress.model.User;

public interface UserRepository extends CrudRepository<User,Integer> {
	
	@Query(value = SqlQueries.GET_USER_BY_ID, nativeQuery = true)
	User findUserById(int userId);

}
