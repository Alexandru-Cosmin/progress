package com.training.progress.repository;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.training.progress.model.Task;

public interface TaskRepository extends CrudRepository<Task,Integer> {
	
	@Query(value = SqlQueries.FIND_PERCENTAGE_OF_COMPLETION, nativeQuery = true)
	public int findPercentageOfCompletion(int id);
	
	@Query(value = SqlQueries.MARK_COMPLETION_OF_TASK, nativeQuery = true)
	public void setTaskCompleted(int percentageOfCompletion ,int id);

	@Modifying
	@Transactional
	@Query(value = SqlQueries.ADD_TASK, nativeQuery = true)
	public void addTask(String name, String description);

	/*@Query(value = "from Task t join TaskToUser tu on tu.taskId=t.id where tu.userId in (:userIds)")
	public List<Task> getTasksForUsers(List<Integer> userIds);*/

}
