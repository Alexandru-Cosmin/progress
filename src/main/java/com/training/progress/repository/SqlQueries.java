package com.training.progress.repository;

public interface SqlQueries {
	String GET_USERS = "SELECT * FROM workschema.user";

	String GET_TASKS_BY_ID_LIST = "SELECT * FROM workschema.task where id IN (:ids)";

	String FIND_IDS_OF_TASKS_OF_USER = "SELECT task_id FROM workschema.task_to_user "
			+ " WHERE workschema.task_to_user.user_id= ?";

	// JPA
	String FIND_PERCENTAGE_OF_COMPLETION = "SELECT workschema.task.percentage_of_completion FROM "
			+ "workschema.task where workschema.task.id= ?";

	String MARK_COMPLETION_OF_TASK = "UPDATE workschema.task SET "
			+ "percentage_of_completion = ? WHERE workschema.task.id= ?";

	String FIND_TASKS_OF_USER = "SELECT * FROM workschema.task_to_user " + " WHERE workschema.task_to_user.user_id= ?";

	String GET_TASK_FROM_TASKTOUSER_BY_ID = "SELECT * FROM workschema.task_to_user" + " WHERE task_id= ?";

	// JdbcTemplate & JPA
	String FIND_PERCENTAGE_OF_COMPLETION_OF_TASK = "SELECT percentage_of_completion FROM workschema.task_to_user "
			+ " WHERE workschema.task_to_user.user_id= ? AND workschema.task_to_user.task_id= ?";

	String SET_PERCENTAGE_OF_COMPLETION_OF_TASK = "UPDATE workschema.task_to_user SET percentage_of_completion=? "
			+ " WHERE workschema.task_to_user.user_id= ? AND workschema.task_to_user.task_id= ?";

	String GET_USER_BY_ID = "SELECT * FROM workSchema.user u WHERE u.id= ?";
	
	String GET_USER_BY_NAME_AND_PASSWORD = "SELECT * FROM workSchema.user u "
			+ "WHERE u.name= ? AND u.password= ?";

	String ADD_TASK = "INSERT INTO workschema.task "
			+ "(name, description, completed_final_flag) VALUES (?, ?, 'false')";

	// JdbcTemplate
	String DELETE_TASK_BY_ID = "DELETE FROM workschema.task WHERE id= ?";

	String GET_ASSIGNED_AND_COMPLETED_COUNT_FOR_TASK = "SELECT count(*) as assigned,sum(CASE WHEN tu.completed_flag=true THEN 1 ELSE 0 END) as completed"
			+ " FROM workschema.task_to_user tu WHERE task_id=?";

	String GET_TASKS_BY_USER = "SELECT t.*"
			+ " FROM workschema.task_to_user tu JOIN workschema.task t ON tu.task_id = t.id "
			+ "WHERE tu.user_id = ?";
}
