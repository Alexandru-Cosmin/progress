package com.training.progress.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.training.progress.model.TaskToUser;

public interface TaskToUserRepository extends CrudRepository<TaskToUser,Integer>{

	
	
	@Query(value = SqlQueries.FIND_TASKS_OF_USER, nativeQuery = true)
	public ArrayList<TaskToUser> findAllElementsByUserId(int id);

	@Query(value = SqlQueries.FIND_PERCENTAGE_OF_COMPLETION_OF_TASK, nativeQuery = true)
	public int findPercentageOfCompletionOfTask(int userId,int taskId);

	@Modifying
	@Transactional
	@Query(value = SqlQueries.SET_PERCENTAGE_OF_COMPLETION_OF_TASK, nativeQuery = true)
	public void setPercentageOfCompletion(int percentageOfCompletion,int userId, int taskId);

	@Query(value = SqlQueries.GET_TASK_FROM_TASKTOUSER_BY_ID, nativeQuery = true)
	public ArrayList<TaskToUser> checkHowManyTimesTaskWasCompleted(int taskId);
	
	/*@Query(value = SqlQueries.GET_ASSIGNED_AND_COMPLETED_COUNT_FOR_TASK, nativeQuery = true)
	public ArrayList<CounterOfTask> checkHowManyTimesTaskWasCompleted(int taskId);*/
}
