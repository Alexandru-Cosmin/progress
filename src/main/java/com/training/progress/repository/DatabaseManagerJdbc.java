package com.training.progress.repository;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.training.progress.model.Task;
import com.training.progress.model.User;
import com.training.progress.model.dto.CounterOfTask;
import com.training.progress.model.dto.UserDto;

@Component
public class DatabaseManagerJdbc implements DatabaseManager{

	private Logger log = LoggerFactory.getLogger(DatabaseManagerJdbc.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	
	@Override
	public List<User> getUsers() {
		log.info("getUsers called in DBMJdbc");
		List<User> users = new ArrayList<User>();
		try {
			users = (ArrayList<User>)jdbcTemplate.query(SqlQueries.GET_USERS, new Object[] {},
					new BeanPropertyRowMapper<>(User.class));
		}catch(DataAccessException e) {
			log.error(e.getMessage());
		}
		log.info("Users returned in DBMJdbc: "+users.toString());
		return users;
	}

	@Override
	public ArrayList<Task> getTaskById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Task> getTasksByUserId(int id) {
		log.info("getTasksByUserId called in DBMJdbc");
		List<Task> tasksOfUser = new ArrayList<Task>();
		try {
			tasksOfUser	= (ArrayList<Task>)jdbcTemplate.query(SqlQueries.GET_TASKS_BY_USER, new Object[] {id},
					new BeanPropertyRowMapper<>(Task.class));
		}catch(DataAccessException e) {
			log.error(e.getMessage());
		}
		log.info("Tasks returned in DBMJdbc: "+tasksOfUser.toString());
		return tasksOfUser;
	}

	@Override
	public int findPercentageOfCompletion(int userId, int taskId) {
		log.info("getPercentageOfCompletion called in DBMJdbc");
		log.info("Task id: "+taskId);
		int percentageOfCompletion = 0;
		try {
			percentageOfCompletion	= jdbcTemplate.queryForObject(SqlQueries.FIND_PERCENTAGE_OF_COMPLETION_OF_TASK, new Integer[] {userId,taskId},
					Integer.class);
		}catch(DataAccessException e) {
			log.error(e.getMessage());
		}
		log.info("PercentageOfCompletion returned in DBMJdbc: "+percentageOfCompletion);
		return percentageOfCompletion;
	}

	@Override
	public String setPercentageOfCompletion(int userId, int taskId, int percentageOfCompletion) {
		log.info("setPercentageOfCompletion called in DBMJdbc");
		String operationMessage = "success";
		try {
			jdbcTemplate.update(SqlQueries.SET_PERCENTAGE_OF_COMPLETION_OF_TASK, percentageOfCompletion, taskId, userId);
		}catch(DataAccessException e) {
			operationMessage = "fail";
			log.error(e.getMessage());
		}
		return operationMessage;
	}

	@Override
	public String addTask(int userId, String name, String description) {
		log.info("addTask called in DBMJdbc");
		String operationMessage = "success";
		try {
				jdbcTemplate.update(SqlQueries.ADD_TASK, name, description);
		}catch(DataAccessException e) {
			operationMessage = "fail";
			log.error(e.getMessage());
		}
		return operationMessage;
	}

	@Override
	public String deleteTask(int userId, int id) {
		log.info("deleteTask called in DBMJdbc");
		String operationMessage = "success";
		try {
			jdbcTemplate.update(SqlQueries.DELETE_TASK_BY_ID, id);
		}catch(DataAccessException e) {
			operationMessage = "fail";
			log.error(e.getMessage());
		}
		return operationMessage;
	}
	
	@Override
	public String checkHowManyTimesTaskWasCompleted(int userId, int taskId) {
		log.info("checkHowManyTimesTaskWasCompleted called in DBMJdbc");
		String operationMessage = "success";
		try {
				ArrayList<CounterOfTask> counter = (ArrayList<CounterOfTask>) jdbcTemplate.query(SqlQueries.GET_ASSIGNED_AND_COMPLETED_COUNT_FOR_TASK, new Object[] {taskId},
						new BeanPropertyRowMapper<>(CounterOfTask.class));				
				operationMessage = "Task was assigned "+counter.get(0).getAssigned()+" times and was completed "
						+counter.get(0).getCompleted()+" times!";
		}catch(DataAccessException e) {
			operationMessage = "fail";
			log.error(e.getMessage());
		}
		return operationMessage;
	}
	
	@Override
	public boolean userIsAdmin(int userId) {
		log.info("userIsAdmin called in DBMJdbc");
		User user = new User();
		try {
			user = (User)jdbcTemplate.queryForObject(SqlQueries.GET_USER_BY_ID, new Object[] {userId},
					new BeanPropertyRowMapper<>(User.class));
			System.out.println("User: "+user.getId()+user.getName()+user.isAdmin());
		}catch(DataAccessException e) {
			log.error(e.getMessage());
		}
		return user.isAdmin();
	}

	@Override
	public boolean login(UserDto u) {
		log.info("login in DBMJdbc called...");
		User user = null;
		try {
			user = (User)jdbcTemplate.queryForObject(SqlQueries.GET_USER_BY_NAME_AND_PASSWORD, new Object[] {u.getName(),u.getPassword()},
					new BeanPropertyRowMapper<>(User.class));
			if(user != null){
				return true;
			}
		}catch(DataAccessException e) {
			log.error(e.getMessage());
		}
		return false;
	}

}
