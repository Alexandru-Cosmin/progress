package com.training.progress.security;

public class AccountCredentials {

	  private String username;
	  private String password;
	  // getters & setters
	  
	public AccountCredentials(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	public AccountCredentials() {
		super();
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	  
	  
	}